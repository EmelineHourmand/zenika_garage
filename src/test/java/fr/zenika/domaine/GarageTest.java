package fr.zenika.domaine;

import fr.zenika.domaine.service.ElectricalEngine;
import fr.zenika.domaine.service.TermalEngine;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GarageTest {

    private static final Garage garage = new Garage();

    private static final Vehicle vehicle1 = new Vehicle("Renault", "Meganne", 15000, new TermalEngine());
    private static final Vehicle vehicle2 = new Vehicle("Karin", "Sultan Classic", 250000, new TermalEngine());
    private static final Moto moto = new Moto("Shitzu", "Hakuchou Drag", 95000, new ElectricalEngine());
    private static final Auto auto = new Auto("Annis", "Elegy Retro Custom", 195000, new TermalEngine());

    @BeforeAll
    static void setUp() {
        garage.addVehicle(vehicle1);
        garage.addVehicle(vehicle2);
        garage.addVehicle(moto);
        garage.addVehicle(auto);
    }

    @Test
    void set_exit_vehicle() {
        assertEquals(moto, garage.setExitVehicle(moto));
    }

    @Test
    void check_if_garage_have_exit_vehicle() {
        assertNull(garage.getExitVehicle());
        garage.setExitVehicle(auto);
        assertNotNull(garage.getExitVehicle());
    }

    @Test
    void go_back_exit_vehicle() {
        assertNull(garage.goBackExitVehicle());
    }
}