package fr.zenika.cli.screen.garage;

import fr.zenika.domaine.service.manager.GarageManager;
import fr.zenika.cli.ScannerManager;
import fr.zenika.cli.screen.Screen;
import fr.zenika.cli.screen.menu.GarageMenuScreen;
import fr.zenika.cli.screen.menu.MainMenuScreen;
import fr.zenika.domaine.Garage;
import fr.zenika.domaine.Vehicle;

public class OutCarOfGarageScreen implements Screen {

    @Override
    public Screen render() {

        Garage garage = GarageManager.getInstance().getGarage();

        System.out.println("****** | TON GARAGE | ******");

        if (garage.getGarage().size() > 0) {
            if (garage.getExitVehicle() == null) {
                for (int i = 0; i < garage.getGarage().size(); i++) {
                    Vehicle vehicle = garage.getGarage().get(i);
                    System.out.println(vehicle.toString(i));
                }
            } else {
                System.out.println("""
                        T'es déjà en train de rouler ! 
                        Rentre au garage si tu veux sortir un autre véhicule.
                        """);
                return new GarageMenuScreen();
            }

            System.out.println();

        } else {
            System.out.println("""
                    Oh non ! Ton garage est vide ! :( 
                    Vas t'acheter un véhicule !
                    """);

            return new MainMenuScreen();
        }

        System.out.println("- Quel véhicule tu veux sortir de ton garage ?");
        int choice = ScannerManager.getInstance().getCustomerChoice();

        Vehicle vehicle = garage.getGarage().get(choice - 1);
        garage.setExitVehicle(vehicle);
        vehicle.startEngine();
        vehicle.accelerate();

        return new GarageMenuScreen();
    }
}
