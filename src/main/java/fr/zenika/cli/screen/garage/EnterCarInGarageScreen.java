package fr.zenika.cli.screen.garage;

import fr.zenika.domaine.service.manager.GarageManager;
import fr.zenika.cli.screen.Screen;
import fr.zenika.cli.screen.menu.GarageMenuScreen;
import fr.zenika.cli.screen.menu.MainMenuScreen;
import fr.zenika.domaine.Garage;

public class EnterCarInGarageScreen implements Screen {
    @Override
    public Screen render() {

        Garage garage = GarageManager.getInstance().getGarage();

        System.out.println("****** | TON GARAGE | ******");
        if (garage.getGarage().size() > 0) {
            if (garage.getExitVehicle() != null) {
                garage.getExitVehicle().stopEngine();
                garage.setExitVehicle(null);
            } else {
                System.out.println("""
                        Tu es sortis avec aucun véhicule, 
                        comment tu veux rentrer au garage si tu es déjà dedans !!??
                        """);
            }

            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                System.out.println(e);
            }

            return new GarageMenuScreen();

        } else {
            System.out.println("""
                    Oh non ! Ton garage est vide ! :( 
                    Vas t'acheter un véhicule !
                    """);

            return new MainMenuScreen();
        }

    }
}
