package fr.zenika.cli.screen.garage;

import fr.zenika.domaine.service.manager.GarageManager;
import fr.zenika.cli.screen.Screen;
import fr.zenika.cli.screen.menu.MainMenuScreen;
import fr.zenika.domaine.Garage;
import fr.zenika.domaine.Vehicle;

public class GarageScreen implements Screen {
    @Override
    public Screen render() {
        Garage garage = GarageManager.getInstance().getGarage();

        System.out.println("****** | TON GARAGE | ******");
        if (garage.getGarage().size() > 0) {
            for (int i = 0; i < garage.getGarage().size(); i++) {
                Vehicle vehicle = garage.getGarage().get(i);
                System.out.println(vehicle.toString(i));
            }
            System.out.println();
        } else {
            System.out.println("""
                    Oh non ! Ton garage est vide ! :( 
                    Vas t'acheter un véhicule !
                    """);
        }

        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println(e);
        }

        return new MainMenuScreen();
    }
}
