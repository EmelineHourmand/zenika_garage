package fr.zenika.cli.screen.garage;

import fr.zenika.domaine.service.manager.GarageManager;
import fr.zenika.cli.screen.Screen;
import fr.zenika.cli.screen.menu.GarageMenuScreen;
import fr.zenika.domaine.Auto;
import fr.zenika.domaine.Garage;
import fr.zenika.domaine.Moto;
import fr.zenika.domaine.Vehicle;

public class GetCrazyWithVehicleScreen implements Screen {

    @Override
    public Screen render() {
        Garage garage = GarageManager.getInstance().getGarage();

        if (garage.getGarage().size() > 0) {
            if (garage.getExitVehicle() == null) {
                System.out.println("""
                        Sort un véhicule pour faire le fou sur la route !
                        """);
                return new GarageMenuScreen();
            } else {
                Vehicle exitVehicle = garage.getExitVehicle();
                if (exitVehicle instanceof Moto) {
                    ((Moto) exitVehicle).wheeling();
                } else if (exitVehicle instanceof Auto) {
                    ((Auto) exitVehicle).carRadio();
                } else {
                    System.out.println("""
                            Dommage...
                            tu peux pas faire le fou avec ce véhicule !
                            C'est moche hein ?
                            """);
                }
            }


        } else {
            System.out.println("""
                    Oh non ! Ton garage est vide ! :( 
                    Vas t'acheter un véhicule !
                    """);
        }

        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println(e);
        }

        return new GarageMenuScreen();
    }
}



