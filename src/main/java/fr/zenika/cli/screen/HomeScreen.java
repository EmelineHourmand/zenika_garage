package fr.zenika.cli.screen;

import fr.zenika.cli.screen.menu.MainMenuScreen;

public class HomeScreen implements Screen {

    @Override
    public Screen render() {
        System.out.println("""
                  ****** | BIENVENU ! | ******
                Ici tu es dans ton super garage !
                """);

        return new MainMenuScreen();
    }

}
