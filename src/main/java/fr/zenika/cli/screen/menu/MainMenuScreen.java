package fr.zenika.cli.screen.menu;

import fr.zenika.cli.ScannerManager;
import fr.zenika.cli.screen.*;
import fr.zenika.cli.screen.cardealer.BuyCarScreen;
import fr.zenika.cli.screen.garage.GarageScreen;

public class MainMenuScreen implements Screen {

    @Override
    public Screen render() {

        System.out.println("""
        ****** | MENU PRINCIPAL | ******
        1) Acheter un véhicule au concessionnaire.
        2) Voir mon garage.
        3) Interagir avec tes véhicules.
        4) Quitter l'application.
        """);

        System.out.println("- Que souhaitez-vous faire ?");

        int choice = ScannerManager.getInstance().getCustomerChoice();

        return switch (choice) {
            case 1 -> new BuyCarScreen();
            case 2 -> new GarageScreen();
            case 3 -> new GarageMenuScreen();
            case 4 -> new EndScreen();
            default -> this;
        };
    }
}
