package fr.zenika.cli.screen.menu;

import fr.zenika.cli.ScannerManager;
import fr.zenika.cli.screen.garage.EnterCarInGarageScreen;
import fr.zenika.cli.screen.garage.GetCrazyWithVehicleScreen;
import fr.zenika.cli.screen.garage.OutCarOfGarageScreen;
import fr.zenika.cli.screen.Screen;

public class GarageMenuScreen implements Screen {

    @Override
    public Screen render() {

        System.out.println("""
                ****** | MENU GARAGE | ******
                1) Sortir ton véhicule du garage.
                2) Faire le fou avec ton véhicule.
                3) Rentrer ton véhicule dans le garage.
                4) Menu principal.
                """);

        System.out.println("- Que souhaite-tu faire ?");

        int choice = ScannerManager.getInstance().getCustomerChoice();

        return switch (choice) {
            case 1 -> new OutCarOfGarageScreen();
            case 2 -> new GetCrazyWithVehicleScreen();
            case 3 -> new EnterCarInGarageScreen();
            case 4 -> new MainMenuScreen();
            default -> this;
        };
    }
}
