package fr.zenika.cli.screen;

/**
 * @author Emeline Hourmand
 */
public interface Screen {

    /**
     * Affiche un écran dans la console et retourne le prochain écran à afficher.
     * @return Le prochain écran ou null si l'application doit être stoppé.
     */
    Screen render();

    /**
     * Retourne le premier écran à afficher.
     * @param firstScreen = premier écran à afficher.
     */
    static void displayFirstScreen(Screen firstScreen) {
        Screen screenToDisplay = firstScreen;
        while (screenToDisplay != null) {
            screenToDisplay = screenToDisplay.render();

            if (screenToDisplay != null) {
                System.out.println("""       
                                *-----------*
                        """);
            }
        }
    }
}
