package fr.zenika.cli.screen;

public class EndScreen implements Screen{

    @Override
    public Screen render() {
        System.out.println("****** | FERMETURE DU GARAGE | ******");
        return null;
    }
}
