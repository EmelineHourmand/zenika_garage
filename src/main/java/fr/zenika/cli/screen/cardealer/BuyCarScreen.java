package fr.zenika.cli.screen.cardealer;

import fr.zenika.domaine.service.manager.GarageManager;
import fr.zenika.cli.ScannerManager;
import fr.zenika.cli.screen.Screen;
import fr.zenika.cli.screen.menu.MainMenuScreen;
import fr.zenika.domaine.Auto;
import fr.zenika.domaine.Garage;
import fr.zenika.domaine.Moto;
import fr.zenika.domaine.Vehicle;
import fr.zenika.domaine.service.ElectricalEngine;
import fr.zenika.domaine.service.TermalEngine;

import java.util.List;

public class BuyCarScreen implements Screen {

    private static final List<Vehicle> carDealer = List.of(
            new Vehicle("Renault", "Meganne", 15000, new TermalEngine()),
            new Vehicle("Karin", "Sultan Classic", 250000, new TermalEngine()),
            new Moto("Shitzu", "Hakuchou Drag", 95000, new ElectricalEngine()),
            new Auto("Annis", "Elegy Retro Custom", 195000, new TermalEngine())
    );

    @Override
    public Screen render() {

        System.out.println("""
                      ****** | CONCESSIONNAIRE | ******
                - Voici la liste de nos véhicules à vendre :
                """);

        Garage garage = GarageManager.getInstance().getGarage();

        for (int i = 0; i < carDealer.size(); i++) {
            Vehicle vehicle = carDealer.get(i);
            int availableChoice = i + 1;
            System.out.println(availableChoice + ". "
                    + vehicle.getMark() + " "
                    + vehicle.getModel() + " Prix : "
                    + vehicle.getPrice() + "€.");
        }

        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("- Quel véhicule souhaitez-vous acheter ?");
        int choice = ScannerManager.getInstance().getCustomerChoice();

        if (choice > 0 && choice <= carDealer.size()) {
            garage.addVehicle(carDealer.get(choice - 1));

            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e);
            }

            System.out.println("- Tu as acheté le véhicule ! -");
            System.out.println();

            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                System.out.println(e);
            }

            return new MainMenuScreen();
        } else {
            System.out.println("Choix incorrect.");
            return this;
        }
    }
}
