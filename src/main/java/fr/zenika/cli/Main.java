package fr.zenika.cli;

import fr.zenika.cli.screen.HomeScreen;
import fr.zenika.cli.screen.Screen;
import fr.zenika.domaine.Auto;
import fr.zenika.domaine.Moto;
import fr.zenika.domaine.Vehicle;
import fr.zenika.domaine.service.ElectricalEngine;
import fr.zenika.domaine.service.TermalEngine;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    /**
     * @param args String[]
     */
    public static void main(String[] args) {

        Screen.displayFirstScreen(new HomeScreen());

        /*ArrayList<Vehicle> catalog = new ArrayList<>();

        Vehicle vehicle1 = new Vehicle("Renault", "Meganne", 15000, new TermalEngine());
        Vehicle vehicle2 = new Vehicle("Karin", "Sultan Classic", 250000, new TermalEngine());

        Moto moto1 = new Moto("Shitzu", "Hakuchou Drag", 95000, new ElectricalEngine());
        Auto auto1 = new Auto("Annis", "Elegy Retro Custom", 195000, new TermalEngine());


        catalog.add(vehicle1);
        catalog.add(vehicle2);
        catalog.add(moto1);
        catalog.add(auto1);

        v1(vehicle1);
        v2(vehicle2);
        v3(moto1);
        v4(auto1);
        v5(auto1);

        addToCatalog(catalog);
        getCatalog(catalog);
        buyACar(catalog);
    }

    public static void v1(Vehicle vehicle) {
        vehicle.startEngine();
        vehicle.accelerate();
        vehicle.stopEngine();
    }

    public static void v2(Vehicle vehicle) {
        vehicle.startEngine();
        vehicle.accelerate();
    }

    public static void v3(Moto moto) {
        moto.weeling();
    }

    public static void v4(Auto auto) {
        auto.carRadio();
    }

    public static void v5(Vehicle vehicle) {
        vehicle.buy();
    }

    public static ArrayList<Vehicle> addToCatalog(ArrayList<Vehicle> catalog) {
        Scanner sc= new Scanner(System.in);
        Vehicle newVehicle = new Vehicle();
        System.out.println("AJOUTER UN VEHICULE AU CATALOGUE");
        System.out.println("Entrer la marque : ");
        newVehicle.setMark(sc.nextLine());
        System.out.println("Entrer le model : ");
        newVehicle.setModel(sc.nextLine());
        System.out.println("Entrer un prix : ");
        newVehicle.setPrice(sc.nextInt());

        System.out.println("Vous avez ajouté un nouveau vehicule : "
                + newVehicle.getModel() + " " + newVehicle.getMark() +
                ". Au prix de : " + newVehicle.getPrice() + " $.") ;

        catalog.add(newVehicle);
        System.out.println("\n");
        sc.close();
        return catalog;
    }

    public static void getCatalog(ArrayList<Vehicle> catalog) {
        System.out.println("*_* CATALOGUE *_*");
        for (int y = 0; y < catalog.size(); y++) {
            System.out.println(y+1 + ". " + catalog.get(y).getMark() +
                    " " + catalog.get(y).getModel() + " " + catalog.get(y).getPrice() + " $." );
        }
    }

    public static void buyACar(ArrayList<Vehicle> catalog) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrer le véhicule que vous voulez acheter : ");
        String name = sc.nextLine();
        for (int i = 0; i < catalog.size(); i++) {
            if (catalog.get(i).getModel().equals(name)) {
                System.out.println("Je veux acheter la " + catalog.get(i).getModel() + " pour la somme de " + catalog.get(i).getPrice() + " $.");
                return;
            }
        }
    }

         */
    }
}
