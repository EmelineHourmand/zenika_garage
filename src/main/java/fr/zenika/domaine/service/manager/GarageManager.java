package fr.zenika.domaine.service.manager;

import fr.zenika.domaine.Garage;

/**
 * @author Emeline Hourmand
 * Singleton pour la gestion du garage de l'utilisateur.
 */
public class GarageManager {

    private static final GarageManager INSTANCE = new GarageManager();
    private final Garage garage = new Garage();

    /**
     * Constructeur privé pour ne pas créer de nouvelles instances de garage.
     */
    private GarageManager() {
    }

    /**
     * Récupère l'instance garage de l'utilisateur.
     * @return l'instance garage de l'utilisateur.
     */
    public static GarageManager getInstance() {
        return INSTANCE;
    }

    /**
     * Permet de récupérer le garage de l'utilisateur instancié.
     * @return le commande du client.
     */
    public Garage getGarage() {
        return garage;
    }

}
