package fr.zenika.domaine.service;

/**
 * @author Emeline Hourmand
 * Interface de moteur.
 */
public interface Engine {

    /**
     * Methode qui permet de démarrer un moteur.
     */
    void start();

    /**
     * Methode qui permet de stopper un moteur.
     */
    void stop();
}
