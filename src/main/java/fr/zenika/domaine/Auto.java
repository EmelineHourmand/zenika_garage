package fr.zenika.domaine;

import fr.zenika.domaine.service.Engine;

public class Auto extends Vehicle {

    public Auto(String mark, String model, int price, Engine engine) {
        super(mark, model, price, engine);
    }

    public void carRadio() {
        System.out.println("J'allume la radio de ma " + getModel() + " sur 105.67 FM.");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("La lala lalala liiiii ~");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
