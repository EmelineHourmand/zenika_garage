package fr.zenika.domaine;

import java.util.ArrayList;
import java.util.List;

public class Garage {

    private List<Vehicle> garage = new ArrayList<>();
    private Vehicle exitVehicle = null;

    public void addVehicle(Vehicle vehicle) {
        garage.add(vehicle);
    }

    public void removeVehicle(Vehicle vehicle) {
        garage.remove(vehicle);
    }

    public List<Vehicle> getGarage() {
        return garage;
    }

    public Vehicle getExitVehicle() {
        return exitVehicle;
    }

    public Vehicle setExitVehicle(Vehicle vehicle) {
            return this.exitVehicle = vehicle;
    }

    public Vehicle goBackExitVehicle() {
       return exitVehicle = null;
    }

}
