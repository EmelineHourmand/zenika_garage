package fr.zenika.domaine;

import fr.zenika.domaine.service.Engine;

/**
 * @author Emeline Hourmand
 */
public class Vehicle {

    private String mark;
    private String model;
    private int price;
    Engine engine;

    /**
     * Methode qui permet de démarrer un véhicule.
     */
    public void startEngine() {
        try {
            System.out.println("Je démarre la " +mark + " " + model + ".");
            Thread.sleep(1000);
            engine.start();
            Thread.sleep(500);
            System.out.println("Je sors ma " + mark + " " + model + " du garage.");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Methode qui permet de stopper un véhicule.
     */
    public void stopEngine() {
        try {
            System.out.println("Je freine.");
            Thread.sleep(1000);
            System.out.println("Voila, arrêté et garé dans le garage !");
            Thread.sleep(1000);
            engine.stop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Methode qui permet a un véhicule d'accélérer.
     */
    public void accelerate() {
        try {
            if (getModel().equals("Meganne")) {
                System.out.println("J'accélère !");
                Thread.sleep(1000);
                System.out.println("Je roule tranquille.");
                Thread.sleep(1000);
            } else {
                System.out.println("J'accélère !");
                Thread.sleep(1000);
                System.out.println("50 km/h");
                Thread.sleep(1000);
                System.out.println("100 km/h");
                Thread.sleep(1000);
                System.out.println("150 km/h !");
                Thread.sleep(500);
                System.out.println("Je suis trop dingo !!");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    //TODO Achat de véhicule.
    public void buy() {
        try {
            System.out.println("Moi : Bonjour ! Je voudrais acheter une " + model + "!");
            Thread.sleep(1000);
            System.out.println("Vendeur : Elle est au prix de " + price + " $.");
            Thread.sleep(1000);
            if (price < 20000) {
                System.out.println("Moi : J'achète !");
            } else {
                for (int i = 0; i < 5; i++) {
                    Thread.sleep((500));
                    System.out.println("Moi : ...");
                }
                System.out.println("Moi : WOUHA TROP CHERE !");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructeur de véhicule.
     * @param mark La marque du véhicule.
     * @param model Le modèle du véhicule.
     * @param price Le prix du véhicule.
     * @param engine Le type de moteur du véhicule.
     */
    public Vehicle(String mark, String model, int price, Engine engine) {
        this.mark = mark;
        this.model = model;
        this.price = price;
        this.engine = engine;
    }

    /**
     * Récupère la marque du véhicule.
     * @return la marque du véhicule.
     */
    public String getMark() {
        return mark;
    }

    /**
     * Permet de saisir la marque du véhicule.
     * @param mark marque du véhicule.
     */
    public void setMark(String mark) {
        this.mark = mark;
    }

    /**
     * Récupère le modèle du véhicule.
     * @return le modèle du véhicule.
     */
    public String getModel() {
        return model;
    }

    /**
     * Permet de saisir le modèle du véhicule.
     * @param model modèle du véhicule.
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Récupère le prix du véhicule.
     * @return le prix du véhicule.
     */
    public int getPrice() {
        return price;
    }

    /**
     * Permet de saisir le prix du véhicule.
     * @param price prix du véhicule.
     */
    public void setPrice(int price) {
        this.price = price;
    }


    public String toString(int i) {
        int id = i + 1;
        return id + ". " + mark
                + " " + model + ".";
    }

}
