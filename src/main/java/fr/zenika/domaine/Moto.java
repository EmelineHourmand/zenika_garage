package fr.zenika.domaine;

import fr.zenika.domaine.service.Engine;

public class Moto extends Vehicle {

    public Moto(String mark, String model, int price, Engine engine) {
        super(mark, model, price, engine);
    }

    public void wheeling() {
        System.out.println("Je met ma " + getModel() + " en Y ! Po ! Po ! Po !");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
